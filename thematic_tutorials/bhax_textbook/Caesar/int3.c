#include <stdio.h>
#include <stdlib.h>

int
main ()
{
	int ***tmm;
	if ((tmm= (int ***) malloc (sizeof (int **))) == NULL)
    {
        return -1;
    }
	
    int nr = 5;
    
    printf("%p\n", &tmm[0]);
    
    if ((tmm[0] = (int **) malloc (nr * sizeof (int *))) == NULL)
    {
        return -1;
    }

    printf("%p\n", tmm[0]);
    
    for (int i = 0; i < nr; ++i)
    {
        if ((tmm[0][i] = (int *) malloc ((i + 1) * sizeof (int))) == NULL)
        {
            return -1;
        }

    }

    printf("%p\n", tmm[0][0]);    
    
    for (int i = 0; i < nr; ++i)
        for (int j = 0; j < i + 1; ++j)
            tmm[0][i][j] = i * (i + 1) / 2 + j;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%i, ", tmm[0][i][j]);
        printf ("\n");
    }

    tmm[0][3][0] = 42.0;
    (*(tmm[0] + 3))[1] = 43.0;	// mi van, ha itt hiányzik a külső ()
    *(tmm[0][3] + 2) = 44.0;
    *(*(tmm[0] + 3) + 3) = 45.0;

    for (int i = 0; i < nr; ++i)
    {
        for (int j = 0; j < i + 1; ++j)
            printf ("%i, ", tmm[0][i][j]);
        printf ("\n");
    }

    for (int i = 0; i < nr; ++i)
        free (tmm[0][i]);

    free (tmm[0]);

    return 0;
}
