#include <stdio.h>

int main()
{
  int a = 12;
  int b = 34;
  printf("Eredeti: a=%i b=%i\n", a, b);
  int c = a;
  a = b;
  b = c;
  printf("Csere segédváltozóval: a=%i b=%i\n\n", a, b);

  a = 123;
  b = 456;
  printf("Eredeti: a=%i b=%i\n", a, b);
  b = b - a;
  a = a + b;
  b = a - b;
  printf("Csere segédváltozó nélkül: a=%i b=%i\n\n", a, b);

  a = 1234;
  b = 4321;
  printf("Eredeti: a=%i b=%i\n", a, b);
  a = a ^ b;
  b = a ^ b;
  a = a ^ b;
  printf("Csere xor-al: a=%i b=%i\n\n", a, b);

  return 0;
}