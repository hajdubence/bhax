#include <stdio.h>
#include <stdlib.h>
#include <curses.h>
#include <unistd.h>

int main(void)
{
    WINDOW *ablak;
    ablak = initscr();

    int x = 0;
    int y = 0;

    int xnov = 1;
    int ynov = 1;

    int mx;
    int my;

    for (;;)
    {
        getmaxyx(ablak, my, mx);

        mvprintw(y, x, "O");

        refresh();
        usleep(100000);

        clear();

        xnov *= abs((x + xnov) / mx) * -2 + 1;          //jobb
        ynov *= abs((y + ynov) / my) * -2 + 1;          //lent
        xnov *= abs((x + xnov - mx + 1) / mx) * -2 + 1; //bal
        ynov *= abs((y + ynov - my + 1) / my) * -2 + 1; //fent

        x = x + xnov;
        y = y + ynov;
    }

    return 0;
}

//gcc labda1.c -o labda1 -lncurses