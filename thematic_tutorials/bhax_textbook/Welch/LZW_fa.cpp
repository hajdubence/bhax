#include <iostream>
#include <string>
#include <math.h>
using namespace std;

class elem
{
public:
	int szam;
	elem* jobb;
	elem* bal;
	elem()
	{
		this->jobb = NULL;
		this->bal = NULL;
	}
};

int Agszam(elem* elemp)
{
	int n = 0;
	if (elemp->bal == NULL && elemp->jobb == NULL)
		return 1;
	if (elemp->bal != NULL)
		n += Agszam(elemp->bal);
	if (elemp->jobb != NULL)
		n += Agszam(elemp->jobb);
	return n;
}

int Elemszam(elem* elemp)
{
	int sum = 1;
	if (elemp->bal != NULL)
		sum += Elemszam(elemp->bal);
	if (elemp->jobb != NULL)
		sum += Elemszam(elemp->jobb);
	return sum;
}

int Aghosz_max(elem* elemp, int hosz)
{
	int jobb = 0, bal = 0;
	if (elemp->bal == NULL && elemp->jobb == NULL)
		return hosz;
	if (elemp->bal != NULL)
		jobb = Aghosz_max(elemp->bal, hosz + 1);
	if (elemp->jobb != NULL)
		bal = Aghosz_max(elemp->jobb, hosz + 1);
	if (jobb>bal)
		return jobb;
	else
		return bal;
}

void Kiir(elem* elemp, string ag)
{
	if (elemp->szam != -1)
		ag += to_string(elemp->szam);
	if (elemp->bal != NULL)
		Kiir(elemp->bal, ag);
	if (elemp->jobb != NULL)
		Kiir(elemp->jobb, ag);
	if (elemp->bal == NULL && elemp->jobb == NULL)
		cout << ag << '\n';
}

void Preorder(elem* elemp)
{
	cout << elemp->szam << ' ';
	if (elemp->bal != NULL)
		Preorder(elemp->bal);
	if (elemp->jobb != NULL)
		Preorder(elemp->jobb);
}

void Inorder(elem* elemp)
{
	if (elemp->bal != NULL)
		Inorder(elemp->bal);
	cout << elemp->szam << ' ';
	if (elemp->jobb != NULL)
		Inorder(elemp->jobb);
}

void Postorder(elem* elemp)
{
	if (elemp->bal != NULL)
		Postorder(elemp->bal);
	if (elemp->jobb != NULL)
		Postorder(elemp->jobb);
	cout << elemp->szam << ' ';
}

int main()
{
	elem* root = new elem;
	root->szam = -1;
	elem* hely = root;
	char be;
	while (cin >> be)
	{
		if (be == '0')
		{
			if (hely->bal == NULL)
			{
				elem* uj = new elem;
				uj->szam = 0;
				hely->bal = uj;
				hely = root;
			}
			else
			{
				hely = hely->bal;
			}
		}
		if (be == '1')
		{
			if (hely->jobb == NULL)
			{
				elem*uj = new elem;
				uj->szam = 1;
				hely->jobb = uj;
				hely = root;
			}
			else
			{
				hely = hely->jobb;
			}
		}
	}

	
	int agszam = Agszam(root);
	cout << "\nA fa ágainak száma: " << agszam;

	int elemszam = Elemszam(root);
	cout << "\nA fa elemszáma: " << elemszam - 1 << " (" << elemszam << ")";

	int aghosz_max = Aghosz_max(root, 0);
	cout << "\nA fa magassága: " << aghosz_max << " (" << aghosz_max + 1 << ")";

	if (agszam<100)
	{
		cout << "\n\nPreorder:\n";
		Preorder(root);
		cout << "\nInorder:\n";
		Inorder(root);
		cout << "\nPostorder:\n";
		Postorder(root);
		cout << "\n\nFa ágai:\n";
		Kiir(root, "");
	}

	return 0;
}
